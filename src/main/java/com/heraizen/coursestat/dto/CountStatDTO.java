package com.heraizen.coursestat.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CountStatDTO {
	private long placedCount;
	private long notPlacedCount;
}
