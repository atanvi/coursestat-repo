package com.heraizen.coursestat.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StudentBasicDataDTO {

		private String name;
		private String qualification;
		private float score;
}
