package com.heraizen.coursestat.service;

import java.util.List;

import com.heraizen.coursestat.domain.Student;
import com.heraizen.coursestat.dto.CountStatDTO;
import com.heraizen.coursestat.dto.StudentBasicDataDTO;

public interface CourseStatService {
	public List<Student> getStudentsByQualfication(String qualification);

	public int getStudentCountByQualification(String qualification);

	public int getPlacedStudentCount();

	public int getNotPlacedStudentCount();

	public CountStatDTO getPlacedAndNotPlacedCount();

	public List<Student> search(String str);

	public float successRate(String batchName);

	public List<Student> maxScoreStudent();

	public List<String> getStudentNames();

	public List<StudentBasicDataDTO> getStudentBasicProfile();
}
