package com.heraizen.coursestat.service;

import java.util.List;
import java.util.stream.Collectors;

import com.heraizen.coursestat.domain.Student;
import com.heraizen.coursestat.dto.CountStatDTO;
import com.heraizen.coursestat.dto.StudentBasicDataDTO;
import com.heraizen.coursestat.util.CsvReaderUtil;

//Make singleton 

public final class CourseStatServiceImpl implements CourseStatService {

	private static CourseStatServiceImpl obj;
	private List<Student> studentList;

	private CourseStatServiceImpl() {
		studentList = CsvReaderUtil.readDataFromFile();
	}

	public static CourseStatServiceImpl getInstance() {
		if (obj == null) {
			synchronized (CourseStatServiceImpl.class) {
				if (obj == null) {
					obj = new CourseStatServiceImpl();
				}
			}
		}
		return obj;
	}

	@Override
	public List<Student> getStudentsByQualfication(String qualification) {

		/*
		 * List<Student> list = new ArrayList<>(); for (Student student : studentList) {
		 * if (student.getQualification().equalsIgnoreCase(qualification)) {
		 * list.add(student); } } return list;
		 */
		return studentList.stream().filter(s -> s.getQualification().equalsIgnoreCase(qualification))
				.collect(Collectors.toList());
	}

	@Override
	public int getStudentCountByQualification(String qualification) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getPlacedStudentCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getNotPlacedStudentCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public CountStatDTO getPlacedAndNotPlacedCount() {

		long total = studentList.size();
		long placedCount = studentList.stream().filter(s -> s.getPStatus().equalsIgnoreCase("y")).count();
		long notPlacedCount = total - placedCount;
		CountStatDTO countDto = CountStatDTO.builder().notPlacedCount(notPlacedCount).placedCount(notPlacedCount)
				.build();
		
		return countDto;
	}

	@Override
	public List<Student> search(String str) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public float successRate(String batchName) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Student> maxScoreStudent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getStudentNames() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<StudentBasicDataDTO> getStudentBasicProfile() {
		// TODO Auto-generated method stub
		return null;
	}

}
