package com.heraizen.coursestat.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Student {

	private String name;
	private String batch;
	private String ccStatus;
	private String pStatus;
	private String qualification;
	private float score;

}
