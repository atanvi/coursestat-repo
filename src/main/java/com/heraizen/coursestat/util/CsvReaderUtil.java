package com.heraizen.coursestat.util;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.heraizen.coursestat.domain.Student;

public final class CsvReaderUtil {

	private static final String FILE_NAME = "student.csv";

	private CsvReaderUtil() {
	}

	public static List<Student> readDataFromFile() {
		List<Student> studentList = new ArrayList<>();
		try {
			List<String> list = Files.readAllLines(Paths.get(ClassLoader.getSystemResource(FILE_NAME).toURI()));
			studentList = list.stream().skip(1).map(row -> convertRowToStudent(row)).collect(Collectors.toList());
		} catch (IOException | URISyntaxException e) {
			System.out.println("While reading csv file: " + e);
		}
		return studentList;
	}

	private static Student convertRowToStudent(String row) {
		String[] data = row.split(",");
		int i = 0;
		String name = data[i++];
		String batch = data[i++];
		String ccStatus = data[i++];
		String pStatus = data[i++];
		String qualification = data[i++];
		float score = Float.parseFloat(data[i++]);
		
		Student student = Student.builder()
				.name(name)
				.pStatus(pStatus)
				.ccStatus(ccStatus)
				.qualification(qualification)
				.score(score)
				.batch(batch).build();
		
		return student;
	}
}
